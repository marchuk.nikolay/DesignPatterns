#pragma once
#include "Prototype.h"

class ConcretePrototype : public Prototype
{
public:
    explicit ConcretePrototype(int id);
    virtual Prototype* Clone();
};