#include "stdafx.h"
#include "ConcretePrototype.h"

ConcretePrototype::ConcretePrototype(int id) :
    Prototype(id)
{
}

Prototype* ConcretePrototype::Clone()
{
    return new ConcretePrototype(*this);
}