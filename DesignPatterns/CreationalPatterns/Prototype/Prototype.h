#pragma once

class Prototype
{
public:
    explicit Prototype(int id);
    virtual Prototype* Clone() = 0;
    virtual ~Prototype() {}

private:
    int id_;
};