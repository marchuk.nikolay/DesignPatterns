// Prototype.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Prototype.h"
#include "ConcretePrototype.h"

int main()
{
    std::unique_ptr<Prototype> prototype(new ConcretePrototype(1));
    std::unique_ptr<Prototype> clone(prototype->Clone());

    return 0;
}