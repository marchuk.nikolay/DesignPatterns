#pragma once

#include "AbstractProductA.h"

class ProductA1 : public AbstractProductA
{
public:
    virtual std::string GetType();
};