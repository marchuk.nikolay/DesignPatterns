#pragma once

#include "AbstractProductA.h"

class AbstractProductB
{
public:
    virtual void Interact(AbstractProductA* productA) = 0;
    virtual ~AbstractProductB() {}
};