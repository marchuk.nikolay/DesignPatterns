#include "stdafx.h"
#include "ConcreteFactory1.h"

AbstractProductA* ConcreteFactory1::GetProductA()
{
    return new ProductA1();
}

AbstractProductB* ConcreteFactory1::GetProductB()
{
    return new ProductB1();
}