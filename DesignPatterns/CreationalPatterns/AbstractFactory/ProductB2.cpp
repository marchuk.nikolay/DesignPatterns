#include "stdafx.h"
#include "ProductB2.h"

void ProductB2::Interact(AbstractProductA* productA)
{
    std::cout << "ProductB2 interacts with " << productA->GetType() << "\n";
}