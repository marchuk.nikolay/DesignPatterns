// AbstractFactory.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Client.h"
#include "ConcreteFactory1.h"
#include "ConcreteFactory2.h"

int main()
{
    std::unique_ptr<AbstractFactory> concreteFactory1(new ConcreteFactory1());
    Client client1(concreteFactory1.get());
    client1.Run();

    std::unique_ptr<AbstractFactory> concreteFactory2(new ConcreteFactory2());
    Client client2(concreteFactory2.get());
    client2.Run();

    return 0;
}