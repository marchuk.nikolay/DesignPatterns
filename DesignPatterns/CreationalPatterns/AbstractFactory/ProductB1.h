#pragma once

#include "AbstractProductB.h"

class ProductB1 : public AbstractProductB
{
public:
    virtual void Interact(AbstractProductA* productA);
};