#include "stdafx.h"
#include "ProductB1.h"

void ProductB1::Interact(AbstractProductA* productA)
{
    std::cout << "ProductB1 interacts with " << productA->GetType() << "\n";
}