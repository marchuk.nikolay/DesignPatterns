#pragma once

#include "AbstractFactory.h"
#include "ProductA2.h"
#include "ProductB2.h"

class ConcreteFactory2 : public AbstractFactory
{
public:
    virtual AbstractProductA* GetProductA();
    virtual AbstractProductB* GetProductB();
};