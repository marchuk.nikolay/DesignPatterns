#include "stdafx.h"
#include "ConcreteFactory2.h"

AbstractProductA* ConcreteFactory2::GetProductA()
{
    return new ProductA2();
}

AbstractProductB* ConcreteFactory2::GetProductB()
{
    return new ProductB2();
}