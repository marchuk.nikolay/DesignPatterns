#pragma once

#include "AbstractProductA.h"

class ProductA2 : public AbstractProductA
{
public:
    virtual std::string GetType();
};