#pragma once

#include "AbstractProductB.h"

class ProductB2 : public AbstractProductB
{
public:
    virtual void Interact(AbstractProductA* productA);
};