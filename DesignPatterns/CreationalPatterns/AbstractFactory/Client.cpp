#include "stdafx.h"
#include "Client.h"

Client::Client(AbstractFactory* factory) :
    productA_(factory->GetProductA()),
    productB_(factory->GetProductB())
{
}

void Client::Run()
{
    productB_->Interact(productA_.get());
}