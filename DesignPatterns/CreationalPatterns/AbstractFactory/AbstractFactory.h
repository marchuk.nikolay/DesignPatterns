#pragma once

#include "AbstractProductA.h"
#include "AbstractProductB.h"

class AbstractFactory
{
public:
    virtual AbstractProductA* GetProductA() = 0;
    virtual AbstractProductB* GetProductB() = 0;
    virtual ~AbstractFactory() {}
};