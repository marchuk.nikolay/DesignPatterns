#pragma once

#include "AbstractFactory.h"
#include "ProductA1.h"
#include "ProductB1.h"

class ConcreteFactory1 : public AbstractFactory
{
public:
    virtual AbstractProductA* GetProductA();
    virtual AbstractProductB* GetProductB();
};