#pragma once

class AbstractProductA
{
public:
    virtual std::string GetType() = 0;
    virtual ~AbstractProductA() {}
};