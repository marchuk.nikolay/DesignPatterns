#pragma once

#include "AbstractFactory.h"
#include "AbstractProductA.h"
#include "AbstractProductB.h"

class Client
{
public:
    explicit Client(AbstractFactory* factory);
    void Run();

private:
    std::unique_ptr<AbstractProductA> productA_;
    std::unique_ptr<AbstractProductB> productB_;
};