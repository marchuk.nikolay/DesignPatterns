#pragma once

class Singleton
{
public:
    static Singleton* GetInstance();
    static int GetId();
    static void SetId(int id);
    Singleton(const Singleton& rhs) = delete;
    Singleton& operator=(Singleton& rhs) = delete;

private:
    Singleton();

private:
    static Singleton* instance_;
    static int id_;
};