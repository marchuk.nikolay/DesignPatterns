// Singleton.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Singleton.h"

int main()
{
    std::unique_ptr<Singleton> singleton1(Singleton::GetInstance());
    std::unique_ptr<Singleton> singleton2(Singleton::GetInstance());

    singleton1->SetId(1);

    std::cout << singleton1->GetId() << "\n";
    std::cout << singleton2->GetId() << "\n";

    singleton2->SetId(2);

    std::cout << singleton1->GetId() << "\n";
    std::cout << singleton2->GetId() << "\n";

    return 0;
}