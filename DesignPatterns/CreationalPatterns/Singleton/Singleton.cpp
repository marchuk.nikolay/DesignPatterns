#include "stdafx.h"
#include "Singleton.h"

Singleton* Singleton::instance_;
int Singleton::id_;

Singleton::Singleton()
{
}

Singleton* Singleton::GetInstance()
{
    if (!instance_)
    {
        instance_ = new Singleton();
    }

    return instance_;
}

int Singleton::GetId()
{
    return id_;
}

void Singleton::SetId(int id)
{
    id_ = id;
}