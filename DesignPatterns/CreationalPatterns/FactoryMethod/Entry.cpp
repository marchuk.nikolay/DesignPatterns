// FactoryMethod.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Creator.h"
#include "ConcreteCreator.h"
#include "Product.h"

int main()
{
    std::unique_ptr<Creator> creator(new ConcreteCreator());
    std::shared_ptr<Product> product(creator->FactoryMethod());

    return 0;
}