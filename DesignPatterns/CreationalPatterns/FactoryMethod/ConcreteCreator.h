#pragma once
#include "Creator.h"

class ConcreteCreator : public Creator
{
public:
    virtual Product* FactoryMethod();
};