#pragma once
#include "Product.h"

class Creator
{
public:
    virtual Product* FactoryMethod() = 0;
    virtual ~Creator() {}
};