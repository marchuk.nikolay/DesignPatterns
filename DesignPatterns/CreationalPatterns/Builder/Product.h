#pragma once

class Product
{
public:
    void Add(const std::string& part);
    void Show();

private:
    std::vector<std::string> parts_;
};