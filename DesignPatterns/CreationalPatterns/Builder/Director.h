#pragma once
#include "Builder.h"

class Director
{
public:
    explicit Director(Builder* builder);
    void Construct();

private:
    std::unique_ptr<Builder> builder_;
};