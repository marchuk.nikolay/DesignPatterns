// Builder.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Builder.h"
#include "ConcreteBuilder.h"
#include "Director.h"
#include "Product.h"

int main()
{
    std::unique_ptr<Builder> builder(new ConcreteBuilder());

    Director director(builder.get());
    director.Construct();

    std::shared_ptr<Product> product(builder->GetProduct());
    product->Show();

    return 0;
}