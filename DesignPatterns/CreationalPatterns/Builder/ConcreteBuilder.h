#pragma once
#include "Builder.h"

class ConcreteBuilder : public Builder
{
public:
    ConcreteBuilder();
    virtual void BuildPartA();
    virtual void BuildPartB();
    virtual void BuildPartC();
    virtual Product* GetProduct();

private:
    std::unique_ptr<Product> product_;
};