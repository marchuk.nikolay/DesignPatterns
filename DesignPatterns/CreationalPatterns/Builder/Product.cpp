#include "stdafx.h"
#include "Product.h"

void Product::Add(const std::string& part)
{
    parts_.push_back(part);
}

void Product::Show()
{
    for (const auto& part : parts_)
    {
        std::cout << part << "\n";
    }
}