#include "stdafx.h"
#include "Director.h"

Director::Director(Builder* builder) :
    builder_(builder)
{
}

void Director::Construct()
{
    builder_->BuildPartA();
    builder_->BuildPartB();
    builder_->BuildPartC();
}