#include "stdafx.h"
#include "ConcreteBuilder.h"

ConcreteBuilder::ConcreteBuilder() :
    product_(new Product())
{
}

void ConcreteBuilder::BuildPartA()
{
    product_->Add("Part A");
}

void ConcreteBuilder::BuildPartB()
{
    product_->Add("Part B");
}

void ConcreteBuilder::BuildPartC()
{
    product_->Add("Part C");
}

Product* ConcreteBuilder::GetProduct()
{
    return product_.get();
}