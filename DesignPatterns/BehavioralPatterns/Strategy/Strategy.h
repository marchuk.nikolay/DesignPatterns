#pragma once

class Strategy
{
public:
    virtual void AlgorythmInterface() = 0;
};