// Strategy.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Context.h"
#include "ConcreteStrategyA.h"
#include "ConcreteStrategyB.h"

int main()
{
    Context contextA(new ConcreteStrategyA());
    contextA.ContextInterface();

    Context contextB(new ConcreteStrategyB());
    contextB.ContextInterface();

    return 0;
}