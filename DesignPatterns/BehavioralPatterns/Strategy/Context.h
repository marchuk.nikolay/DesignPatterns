#pragma once
#include "Strategy.h"

class Context
{
public:
    explicit Context(Strategy* strategy);
    void ContextInterface();

private:
    std::unique_ptr<Strategy> strategy_;
};