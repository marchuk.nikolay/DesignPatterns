#pragma once
#include "Strategy.h"

class ConcreteStrategyA : public Strategy
{
public:
    virtual void AlgorythmInterface();
};