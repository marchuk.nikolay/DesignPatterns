#include "stdafx.h"
#include "Context.h"

Context::Context(Strategy* strategy) :
    strategy_(std::unique_ptr<Strategy>(strategy))
{
}

void Context::ContextInterface()
{
    strategy_->AlgorythmInterface();
}