#pragma once

class Observer
{
public:
    virtual void Update(const std::string& state) = 0;
};