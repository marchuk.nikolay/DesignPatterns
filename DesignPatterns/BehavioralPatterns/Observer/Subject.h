#pragma once
#include "Observer.h"

class Subject
{
public:
    virtual void Attach(Observer* observer);
    virtual void Detach(Observer* observer);
    virtual void Notify(const std::string& state);

private:
    std::vector<Observer*> observers_;
};