#include "stdafx.h"
#include "ConcreteSubject.h"

void ConcreteSubject::ChangeState(const std::string& state)
{
    Notify(state);
}