#pragma once
#include "Subject.h"

class ConcreteSubject : public Subject
{
public:
    void ChangeState(const std::string& state);
};