#pragma once
#include "Observer.h"

class ConcreteObserver : public Observer
{
public:
    explicit ConcreteObserver(const std::string& name);
    virtual void Update(const std::string& state);

private:
    std::string name_;
    std::string state_;
};