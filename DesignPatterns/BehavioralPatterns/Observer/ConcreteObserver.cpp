#include "stdafx.h"
#include "ConcreteObserver.h"

ConcreteObserver::ConcreteObserver(const std::string& name) :
    name_(name), state_("")
{
}

void ConcreteObserver::Update(const std::string& state)
{
    state_ = state;
    std::cout << name_ << "'s state was changed. Now it's " << state_ << "\n";
}