// Observer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ConcreteSubject.h"
#include "ConcreteObserver.h"

int main()
{
    ConcreteSubject subject;

    ConcreteObserver observer1("Observer1");
    ConcreteObserver observer2("Observer2");

    subject.Attach(&observer1);
    subject.Attach(&observer2);

    subject.ChangeState("New state");

    subject.Detach(&observer1);

    std::cout << "\n_____________________________________________\n";
    subject.ChangeState("New state(1)");

    return 0;
}