#include "stdafx.h"
#include "Subject.h"

void Subject::Attach(Observer* observer)
{
    observers_.push_back(observer);
}

void Subject::Detach(Observer* observer)
{
    observers_.erase(std::remove(observers_.begin(), observers_.end(), observer), observers_.end());
}

void Subject::Notify(const std::string& state)
{
    for (auto& observer : observers_)
    {
        observer->Update(state);
    }
}