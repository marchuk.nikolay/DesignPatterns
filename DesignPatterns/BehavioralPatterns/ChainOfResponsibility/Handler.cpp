#include "stdafx.h"
#include "Handler.h"

Handler::Handler() :
    next_(nullptr)
{
}

void Handler::SetNext(Handler* next)
{
    next_.reset(next);
}

Handler* Handler::GetNext()
{
    return next_.get();
}

void Handler::Handle(int number)
{
    next_->Handle(number);
}