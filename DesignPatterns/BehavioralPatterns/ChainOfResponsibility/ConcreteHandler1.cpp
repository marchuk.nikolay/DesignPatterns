#include "stdafx.h"
#include "ConcreteHandler1.h"

void ConcreteHandler1::Handle(int number)
{
    if (number == 1)
    {
        std::cout << "ConcreteHandler1\n";
        return;
    }

    auto next = GetNext();
    if (next != nullptr)
    {
        next->Handle(number);
    }
}