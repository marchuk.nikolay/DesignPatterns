#pragma once
#include "Handler.h"

class ConcreteHandler1 : public Handler
{
public:
    virtual void Handle(int number);
};