#include "stdafx.h"
#include "ConcreteHandler2.h"

void ConcreteHandler2::Handle(int number)
{
    if (number == 2)
    {
        std::cout << "ConcreteHandler2\n";
        return;
    }

    auto next = GetNext();
    if (next != nullptr)
    {
        next->Handle(number);
    }
}