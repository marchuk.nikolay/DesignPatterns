#pragma once

class Handler
{
public:
    Handler();
    void SetNext(Handler* next);
    Handler* GetNext();
    virtual void Handle(int number);

private:
    std::unique_ptr<Handler> next_;
};