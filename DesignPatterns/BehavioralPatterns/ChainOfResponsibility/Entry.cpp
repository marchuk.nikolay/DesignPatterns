// ChainOfResponsibility.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ConcreteHandler1.h"
#include "ConcreteHandler2.h"

int main()
{
    std::unique_ptr<Handler> handler1(new ConcreteHandler1());
    handler1->SetNext(new ConcreteHandler2());

    handler1->Handle(1);

    std::cout << "\n____________________________________\n";
    handler1->Handle(2);
    return 0;
}