#include "stdafx.h"
#include "TerminalExpression.h"

void TerminalExpression::Interpret(Context& context)
{
    context.result = context.source[context.position] == context.vocabulary;
}