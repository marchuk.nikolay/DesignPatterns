#pragma once
#include "Context.h"

class AbstractExpression
{
public:
    virtual void Interpret(Context& context) = 0;
    virtual ~AbstractExpression() {}
};