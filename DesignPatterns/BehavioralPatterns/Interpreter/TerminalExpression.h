#pragma once
#include "AbstractExpression.h"

class TerminalExpression : public AbstractExpression
{
public:
    virtual void Interpret(Context& context);
};