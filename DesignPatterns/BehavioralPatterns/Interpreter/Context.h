#pragma once
#include "stdafx.h"

struct Context
{
    Context(const std::string& source, char vocabulary, bool result, int position) :
        source(source), vocabulary(vocabulary), result(result), position(position) {}

    std::string source;
    char vocabulary;
    bool result;
    int position;
};