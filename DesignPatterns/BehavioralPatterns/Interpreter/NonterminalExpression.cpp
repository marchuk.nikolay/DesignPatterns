#include "stdafx.h"
#include "NonterminalExpression.h"
#include "TerminalExpression.h"

void NonterminalExpression::Interpret(Context& context)
{
    if (context.position < context.source.size())
    {
        terminalExpression_ = std::make_unique<TerminalExpression>();
        terminalExpression_->Interpret(context);
        context.position++;

        if (context.result)
        {
            nonterminalExpression_ = std::make_unique<NonterminalExpression>();
            nonterminalExpression_->Interpret(context);
        }
    }
}