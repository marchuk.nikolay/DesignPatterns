#pragma once
#include "AbstractExpression.h"

class NonterminalExpression : public AbstractExpression
{
public:
    virtual void Interpret(Context& context);

private:
    std::unique_ptr<AbstractExpression> nonterminalExpression_;
    std::unique_ptr<AbstractExpression> terminalExpression_;
};