// Interpreter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "NonterminalExpression.h"

int main()
{
    Context context("aaa", 'a', true, 0);

    NonterminalExpression expression;
    expression.Interpret(context);

    std::cout << std::boolalpha << context.result << "\n";

    return 0;
}