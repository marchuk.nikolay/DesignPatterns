#include "stdafx.h"
#include "Command.h"

Command::Command(Receiver* receiver) :
    receiver_(receiver)
{
}

Receiver* Command::GetReceiver()
{
    return receiver_.get();
}