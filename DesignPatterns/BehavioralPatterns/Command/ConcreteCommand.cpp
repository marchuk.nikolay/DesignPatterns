#include "stdafx.h"
#include "ConcreteCommand.h"

ConcreteCommand::ConcreteCommand(Receiver* receiver) :
    Command(receiver)
{
}

void ConcreteCommand::Execute()
{
    GetReceiver()->Action();
}