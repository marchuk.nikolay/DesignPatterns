#pragma once
#include "Command.h"

class ConcreteCommand : public Command
{
public:
    explicit ConcreteCommand(Receiver* receiver);
    virtual void Execute();
};