#include "stdafx.h"
#include "Invoker.h"

Invoker::Invoker(Command* command) :
    command_(command)
{
}

void Invoker::SetCommand(Command* command)
{
    command_.reset(command);
}

void Invoker::Execute()
{
    command_->Execute();
}