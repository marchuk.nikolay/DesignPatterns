#pragma once
#include "Command.h"

class Invoker
{
public:
    explicit Invoker(Command* command);
    void SetCommand(Command* command);
    void Execute();

private:
    std::unique_ptr<Command> command_;
};