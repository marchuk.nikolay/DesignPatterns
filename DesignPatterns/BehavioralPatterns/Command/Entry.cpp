// Command.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ConcreteCommand.h"
#include "Invoker.h"

int main()
{
    Command* command = new ConcreteCommand(new Receiver());

    auto invoker = std::make_unique<Invoker>(command);
    invoker->Execute();

    return 0;
}