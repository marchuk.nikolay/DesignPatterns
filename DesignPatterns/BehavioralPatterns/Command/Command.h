#pragma once
#include "Receiver.h"

class Command
{
public:
    explicit Command(Receiver* receiver);
    Receiver* GetReceiver();
    virtual void Execute() = 0;
    virtual ~Command() {}

private:
    std::unique_ptr<Receiver> receiver_;
};