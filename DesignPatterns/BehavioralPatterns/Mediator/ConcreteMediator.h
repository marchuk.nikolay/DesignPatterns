#pragma once
#include "Mediator.h"

class ConcreteMediator : public Mediator
{
public:
    virtual void DistributeMessage(Colleague* sender, const std::string& message);
    virtual void Register(Colleague* colleague);

private:
    std::vector<Colleague*> colleagues_;
};