#include "stdafx.h"
#include "ConcreteMediator.h"

void ConcreteMediator::DistributeMessage(Colleague* sender, const std::string& message)
{
    for (auto& colleague : colleagues_)
    {
        if (colleague != sender)
        {
            colleague->ReceiveMessage(message);
        }
    }
}

void ConcreteMediator::Register(Colleague* colleague)
{
    colleagues_.push_back(colleague);
}