// Mediator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ConcreteMediator.h"
#include "ConcreteColleague.h"

int main()
{
    ConcreteColleague colleague1("Colleague1");
    ConcreteColleague colleague2("Colleague2");
    ConcreteColleague colleague3("Colleague3");

    ConcreteMediator mediator;
    mediator.Register(&colleague1);
    mediator.Register(&colleague2);
    mediator.Register(&colleague3);

    colleague1.SendMessage(&mediator, "Message from Colleague1");

    return 0;
}