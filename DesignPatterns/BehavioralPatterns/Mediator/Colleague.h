#pragma once
#include "Mediator.h"

class Mediator;

class Colleague
{
public:
    virtual void SendMessage(Mediator* mediator, const std::string& message) = 0;
    virtual void ReceiveMessage(const std::string& message) = 0;
};