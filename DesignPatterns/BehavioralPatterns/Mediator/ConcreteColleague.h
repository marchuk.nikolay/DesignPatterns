#pragma once
#include "Colleague.h"

class ConcreteColleague : public Colleague
{
public:
    explicit ConcreteColleague(const std::string& name);
    virtual void SendMessage(Mediator* mediator, const std::string& message);
    virtual void ReceiveMessage(const std::string& message);

private:
    std::string name_;
};