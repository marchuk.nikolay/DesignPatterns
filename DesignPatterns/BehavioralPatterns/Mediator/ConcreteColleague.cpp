#include "stdafx.h"
#include "ConcreteColleague.h"

ConcreteColleague::ConcreteColleague(const std::string& name) :
    name_(name)
{
}

void ConcreteColleague::SendMessage(Mediator* mediator, const std::string& message)
{
    mediator->DistributeMessage(this, message);
}

void ConcreteColleague::ReceiveMessage(const std::string& message)
{
    std::cout << name_ << " received " << message << "\n";
}