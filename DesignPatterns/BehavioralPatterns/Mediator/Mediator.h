#pragma once
#include "Colleague.h"

class Colleague;

class Mediator
{
public:
    virtual void DistributeMessage(Colleague* sender, const std::string& message) = 0;
    virtual void Register(Colleague* colleague) = 0;
};