// TemplateMethod.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ConcreteClass.h"

int main()
{
    std::unique_ptr<AbstractClass> instance(std::make_unique<ConcreteClass>());
    instance->TemplateMethod();

    return 0;
}