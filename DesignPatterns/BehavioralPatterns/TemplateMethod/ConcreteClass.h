#pragma once
#include "AbstractClass.h"

class ConcreteClass : public AbstractClass
{
public:
    virtual void PrimitiveOperation1();
    virtual void PrimitiveOperation2();
};