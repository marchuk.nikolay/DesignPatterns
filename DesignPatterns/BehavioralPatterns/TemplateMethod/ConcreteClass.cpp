#include "stdafx.h"
#include "ConcreteClass.h"

void ConcreteClass::PrimitiveOperation1()
{
    std::cout << "PrimitiveOperation1\n";
}

void ConcreteClass::PrimitiveOperation2()
{
    std::cout << "PrimitiveOperation2\n";
}