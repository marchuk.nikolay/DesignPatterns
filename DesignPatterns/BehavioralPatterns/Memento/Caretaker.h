#pragma once
#include "Memento.h"

class Caretaker
{
public:
    Memento* GetMemento();
    void SetMemento(Memento* memento);

private:
    std::unique_ptr<Memento> memento_;
};