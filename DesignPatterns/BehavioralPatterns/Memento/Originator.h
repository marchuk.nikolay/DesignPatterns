#pragma once
#include "Memento.h"

class Originator
{
public:
    Memento* CreateMemento();
    void SetMemento(Memento* memento);
    const std::string& GetState();
    void SetState(const std::string& state);

private:
    std::string state_;
};