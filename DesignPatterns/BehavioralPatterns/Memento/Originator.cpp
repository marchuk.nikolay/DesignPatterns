#include "stdafx.h"
#include "Originator.h"

Memento* Originator::CreateMemento()
{
    return new Memento(state_);
}

void Originator::SetMemento(Memento* memento)
{
    state_ = memento->GetState();
}

const std::string& Originator::GetState()
{
    return state_;
}

void Originator::SetState(const std::string& state)
{
    state_ = state;
}