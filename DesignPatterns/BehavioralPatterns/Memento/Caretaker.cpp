#include "stdafx.h"
#include "Caretaker.h"

Memento* Caretaker::GetMemento()
{
    return memento_.get();
}

void Caretaker::SetMemento(Memento* memento)
{
    memento_.reset(memento);
}