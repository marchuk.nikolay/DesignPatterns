#include "stdafx.h"
#include "Memento.h"

Memento::Memento(const std::string& state) :
    state_(state)
{
}

const std::string& Memento::GetState()
{
    return state_;
}