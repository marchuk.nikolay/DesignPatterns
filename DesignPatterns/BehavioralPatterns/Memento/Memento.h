#pragma once

class Memento
{
public:
    explicit Memento(const std::string& state);
    const std::string& GetState();

private:
    std::string state_;
};