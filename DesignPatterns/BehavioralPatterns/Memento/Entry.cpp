// Memento.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Originator.h"
#include "Memento.h"
#include "Caretaker.h"

int main()
{
    Originator originator;
    originator.SetState("On");

    Caretaker caretaker;
    caretaker.SetMemento(originator.CreateMemento());

    originator.SetState("Off");
    originator.SetMemento(caretaker.GetMemento());

    return 0;
}