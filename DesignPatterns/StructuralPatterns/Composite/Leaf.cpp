#include "stdafx.h"
#include "Leaf.h"

Leaf::Leaf(int id) :
    id_(id)
{
}

void Leaf::Operation()
{
    std::cout << id_ << "\n";
}

void Leaf::Add(const std::shared_ptr<Component>& component)
{
    throw std::exception("Invalid operation\n");
}