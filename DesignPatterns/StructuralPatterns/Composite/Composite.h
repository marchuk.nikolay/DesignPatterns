#pragma once
#include "Component.h"

class Composite : public Component
{
public:
    virtual void Operation();
    virtual void Add(const std::shared_ptr<Component>& component);

private:
    std::vector<std::shared_ptr<Component>> children_;
};