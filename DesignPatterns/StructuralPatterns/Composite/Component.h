#pragma once

class Component
{
public:
    virtual void Operation() = 0;
    virtual void Add(const std::shared_ptr<Component>& component) = 0;
    virtual ~Component() {}
};