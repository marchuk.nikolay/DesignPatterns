// Composite.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Component.h"
#include "Leaf.h"
#include "Composite.h"

int main()
{
    std::shared_ptr<Component> leaf1(new Leaf(1));
    std::shared_ptr<Component> leaf2(new Leaf(2));

    std::shared_ptr<Component> root(new Composite());
    std::shared_ptr<Component> branch1(new Composite());

    branch1->Add(leaf1);
    root->Add(branch1);
    root->Add(leaf2);

    root->Operation();

    return 0;
}