#pragma once
#include "Component.h"

class Leaf : public Component
{
public:
    explicit Leaf(int id);
    virtual void Operation();
    virtual void Add(const std::shared_ptr<Component>& component);

private:
    int id_;
};