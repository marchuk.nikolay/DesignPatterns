#include "stdafx.h"
#include "Composite.h"

void Composite::Operation()
{
    for (auto& child : children_)
    {
        child->Operation();
    }
}

void Composite::Add(const std::shared_ptr<Component>& component)
{
    children_.push_back(component);
}