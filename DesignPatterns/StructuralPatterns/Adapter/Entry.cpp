// Adapter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Target.h"
#include "Adapter.h"

int main()
{
    std::unique_ptr<Target> target(new Adapter());
    target->Request();

    return 0;
}