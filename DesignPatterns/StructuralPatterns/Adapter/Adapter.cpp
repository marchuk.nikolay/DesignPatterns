#include "stdafx.h"
#include "Adapter.h"

Adapter::Adapter() :
    adaptee_(new Adaptee())
{
}

void Adapter::Request()
{
    adaptee_->SpecificRequest();
}