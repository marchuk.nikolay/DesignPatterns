#pragma once
#include "Target.h"
#include "Adaptee.h"

class Adapter : public Target
{
public:
    Adapter();
    virtual void Request();

private:
    std::unique_ptr<Adaptee> adaptee_;
};