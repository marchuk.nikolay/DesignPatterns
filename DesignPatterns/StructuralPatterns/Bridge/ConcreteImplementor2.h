#pragma once
#include "Implementor.h"

class ConcreteImplementor2 : public Implementor
{
public:
    virtual void OperationImp();
};