#pragma once
#include "Abstraction.h"

class RefinedAbstraction : public Abstraction
{
public:
    explicit RefinedAbstraction(Implementor* implementor);
    void Operation() override;
};