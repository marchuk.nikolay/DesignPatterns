// Bridge.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Abstraction.h"
#include "RefinedAbstraction.h"
#include "ConcreteImplementor1.h"
#include "ConcreteImplementor2.h"

int main()
{
    std::unique_ptr<Abstraction> abstraction1(new RefinedAbstraction(new ConcreteImplementor1()));
    abstraction1->Operation();

    std::unique_ptr<Abstraction> abstraction2(new RefinedAbstraction(new ConcreteImplementor2()));
    abstraction2->Operation();

    return 0;
}