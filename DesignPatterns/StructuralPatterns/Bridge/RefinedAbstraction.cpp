#include "stdafx.h"
#include "RefinedAbstraction.h"

RefinedAbstraction::RefinedAbstraction(Implementor* implementor) :
    Abstraction(implementor)
{
}

void RefinedAbstraction::Operation()
{
    Abstraction::Operation();
}