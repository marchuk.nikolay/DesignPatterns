#pragma once
#include "Implementor.h"

class ConcreteImplementor1 : public Implementor
{
public:
    virtual void OperationImp();
};