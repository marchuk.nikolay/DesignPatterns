#include "stdafx.h"
#include "Abstraction.h"

Abstraction::Abstraction(Implementor* implementor) :
    implementor_(implementor)
{
}

void Abstraction::Operation()
{
    implementor_->OperationImp();
}