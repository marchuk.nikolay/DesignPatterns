#pragma once
#include "Implementor.h"

class Abstraction
{
public:
    explicit Abstraction(Implementor* implementor);
    virtual void Operation() = 0;
    virtual ~Abstraction() {}

private:
    std::unique_ptr<Implementor> implementor_;
};