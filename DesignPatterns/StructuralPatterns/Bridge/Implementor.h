#pragma once

class Implementor
{
public:
    virtual void OperationImp() = 0;
    virtual ~Implementor() {}
};