// Facade.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Facade.h"

int main()
{
    Facade facade;

    facade.MethodA();
    facade.MethodB();

    return 0;
}