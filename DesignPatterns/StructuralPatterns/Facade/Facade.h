#pragma once
#include "SubSystem1.h"
#include "SubSystem2.h"
#include "SubSystem3.h"

class Facade
{
public:
    Facade();
    void MethodA();
    void MethodB();

private:
    std::unique_ptr<SubSystem1> subSystem1_;
    std::unique_ptr<SubSystem2> subSystem2_;
    std::unique_ptr<SubSystem3> subSystem3_;
};