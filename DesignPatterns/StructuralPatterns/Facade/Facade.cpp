#include "stdafx.h"
#include "Facade.h"

Facade::Facade() :
    subSystem1_(new SubSystem1()),
    subSystem2_(new SubSystem2()),
    subSystem3_(new SubSystem3())
{
}


void Facade::MethodA()
{
    std::cout << "Facade::MethodA\n";

    subSystem1_->MethodOne();
    subSystem2_->MethodTwo();

    std::cout << "\n";
}

void Facade::MethodB()
{
    std::cout << "Facade::MethodB\n";

    subSystem2_->MethodTwo();
    subSystem3_->MethodThree();

    std::cout << "\n";
}