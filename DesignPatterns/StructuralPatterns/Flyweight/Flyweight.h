#pragma once

class Flyweight
{
public:
    virtual void Operation(int exstrinsicState) = 0;
    virtual ~Flyweight() {};
};