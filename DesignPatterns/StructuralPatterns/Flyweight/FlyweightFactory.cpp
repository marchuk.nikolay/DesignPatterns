#include "stdafx.h"
#include "FlyweightFactory.h"
#include "ConcreteFlyweight.h"

FlyweightFactory::FlyweightFactory()
{
    flyweights_["1"] = std::shared_ptr<Flyweight>(new ConcreteFlyweight());
    flyweights_["2"] = std::shared_ptr<Flyweight>(new ConcreteFlyweight());
    flyweights_["3"] = std::shared_ptr<Flyweight>(new ConcreteFlyweight());
}

Flyweight* FlyweightFactory::GetFlyweight(const std::string& key)
{
    if (flyweights_.find(key) == flyweights_.end())
    {
        flyweights_[key] = std::shared_ptr<Flyweight>(new ConcreteFlyweight());
    }

    return flyweights_.at(key).get();
}