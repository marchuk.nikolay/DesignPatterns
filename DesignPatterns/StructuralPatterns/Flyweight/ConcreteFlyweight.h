#pragma once
#include "Flyweight.h"

class ConcreteFlyweight : public Flyweight
{
public:
    virtual void Operation(int exstrinsicState);

private:
    int intristicState_;
};