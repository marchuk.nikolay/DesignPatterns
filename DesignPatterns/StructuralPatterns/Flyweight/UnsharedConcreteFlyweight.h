#pragma once
#include "Flyweight.h"

class UnsharedConcreteFlyweight : public Flyweight
{
public:
    virtual void Operation(int exstrinsicState);

private:
    int allState_;
};