// Flyweight.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "FlyweightFactory.h"
#include "UnsharedConcreteFlyweight.h"

int main()
{
    std::unique_ptr<FlyweightFactory> factory(new FlyweightFactory());

    Flyweight* flyweight(factory->GetFlyweight("1"));
    flyweight->Operation(0);

    flyweight = factory->GetFlyweight("10");
    flyweight->Operation(0);

    std::unique_ptr<Flyweight> unsharedFlyweight(new UnsharedConcreteFlyweight());
    flyweight = unsharedFlyweight.get();
    flyweight->Operation(0);

    return 0;
}