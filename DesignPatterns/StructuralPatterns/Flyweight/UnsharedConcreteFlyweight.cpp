#include "stdafx.h"
#include "UnsharedConcreteFlyweight.h"

void UnsharedConcreteFlyweight::Operation(int exstrinsicState)
{
    allState_ = exstrinsicState;
    std::cout << "AllState was changed " << allState_ << "\n";
}