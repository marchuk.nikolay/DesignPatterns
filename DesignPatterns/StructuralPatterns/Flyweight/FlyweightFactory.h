#pragma once
#include "Flyweight.h"

class FlyweightFactory
{
public:
    FlyweightFactory();
    Flyweight* GetFlyweight(const std::string& key);

private:
    std::map<std::string, std::shared_ptr<Flyweight>> flyweights_;
};