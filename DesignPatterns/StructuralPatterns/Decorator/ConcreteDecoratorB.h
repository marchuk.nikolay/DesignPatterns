#pragma once
#include "Decorator.h"

class ConcreteDecoratorB : public Decorator
{
public:
    explicit ConcreteDecoratorB(Component* component);
    virtual void Operation();
    void ExtraOperation();
};