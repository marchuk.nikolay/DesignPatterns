#pragma once
#include "Decorator.h"

class ConcreteDecoratorA : public Decorator
{
public:
    explicit ConcreteDecoratorA(Component* component);
    virtual void Operation();
    void ExtraOperation();
};