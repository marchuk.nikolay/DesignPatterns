#include "stdafx.h"
#include "ConcreteDecoratorA.h"

ConcreteDecoratorA::ConcreteDecoratorA(Component* component) :
    Decorator(component)
{
}

void ConcreteDecoratorA::Operation()
{
    Decorator::Operation();
    ExtraOperation();
}

void ConcreteDecoratorA::ExtraOperation()
{
    std::cout << "ConcreteDecoratorA::ExtraOperation\n";
}