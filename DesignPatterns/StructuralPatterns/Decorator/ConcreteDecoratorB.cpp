#include "stdafx.h"
#include "ConcreteDecoratorB.h"

ConcreteDecoratorB::ConcreteDecoratorB(Component* component) :
    Decorator(component)
{
}

void ConcreteDecoratorB::Operation()
{
    Decorator::Operation();
    ExtraOperation();
}

void ConcreteDecoratorB::ExtraOperation()
{
    std::cout << "ConcreteDecoratorB::ExtraOperation\n";
}