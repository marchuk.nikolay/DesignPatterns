#include "stdafx.h"
#include "Decorator.h"

Decorator::Decorator(Component* component) :
    component_(component)
{
}

void Decorator::Operation()
{
    if (component_)
    {
        component_->Operation();
    }
}