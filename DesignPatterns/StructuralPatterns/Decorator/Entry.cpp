// Decorator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Component.h"
#include "ConcreteComponent.h"
#include "ConcreteDecoratorA.h"
#include "ConcreteDecoratorB.h"

int main()
{
    std::unique_ptr<Component> component(new ConcreteComponent());
    std::unique_ptr<Component> decoratorA(new ConcreteDecoratorA(component.get()));
    std::unique_ptr<Component> decoratorB(new ConcreteDecoratorB(decoratorA.get()));

    decoratorB->Operation();
    return 0;
}