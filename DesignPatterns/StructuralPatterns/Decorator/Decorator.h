#pragma once
#include "Component.h"

class Decorator : public Component
{
public:
    explicit Decorator(Component* component);
    virtual void Operation();

private:
    std::unique_ptr<Component> component_;
};