// Proxy.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "RealSubject.h"
#include "Proxy.h"

int main()
{
    std::unique_ptr<Subject> proxy(new Proxy(new RealSubject()));
    proxy->Request();

    return 0;
}