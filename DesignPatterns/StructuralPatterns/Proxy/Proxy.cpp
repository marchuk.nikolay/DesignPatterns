#include "stdafx.h"
#include "Proxy.h"

Proxy::Proxy(RealSubject* realSubject) :
    realSubject_(realSubject)
{
}

void Proxy::Request()
{
    std::cout << "Proxy::Request\n";
    realSubject_->Request();
}