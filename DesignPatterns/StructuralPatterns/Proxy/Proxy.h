#pragma once
#include "RealSubject.h"

class Proxy : public Subject
{
public:
    Proxy(RealSubject* realSubject);
    virtual void Request();

private:
    std::unique_ptr<RealSubject> realSubject_;
};